#Compatibility: Linux session running dbus
#TODO: dbus configuration/suspend
import
    psutil,
    psutil/psutil_linux,
    os,
    dbus,
    strutils,
    strformat,
    sets,
    posix,
    tables,
    algorithm,
    sequtils,
    #osproc,
    jsony

type
    Conf = object
        whitelist*: seq[string]
        notification*: float
        warning*: float
        critical*: float

    ParentInfo = seq[tuple[size: int, pid: int32, paths: HashSet[string], name: string]]
    PidInfo = tuple[pid: int32, name: string, cli: HashSet[string],
            parent: int32, mem: int, sharedm: int]

const
    basic = """{"whitelist":["systemd", "plasmashell"], "critical": 95, "warning": 90, "notification": 85}"""

let
    user = getEnv("USER")
    PAGESIZE = sysconf(SC_PAGE_SIZE) # div 1024
    conffile = getEnv("HOME") & "/.config/memmanage.json"

template errLine(s: string) =
    stderr.write s & "\n"

var
    config: Conf
    whitelist: HashSet[string]


proc parseConf() =
    try:
        config = readFile(conffile).fromJson(Conf)
        errLine "Parsed file: $1." % [conffile]
        errLine config.toJson()
    except:
        errLine "An error happened during parsing config: $1." % [conffile]
        errLine "An example: $1." % [basic]
        errLine "Loading defaults..."
        config = basic.fromJson(Conf)
    finally:
        whitelist = config.whitelist.toHashSet

proc writeConf() =
    try:
        writeFile(conffile, basic)
        errLine "Wrote default config file: $1." % [conffile]
    except:
        errLine "Failed to write default config to $1." % [conffile]
    finally:
        parseConf()

proc loadConf() =
    if fileExists(conffile):
        parseConf()
    else:
        writeConf()

iterator userpids(): PidInfo =
    try:
        for i in pids():
            if try_pid_user(i) == user:
                let
                    info = readFile("/proc/$1/statm" % [$i]).split(" ")
                    shared_mem = info[2].parseInt #* PAGESIZE #div 1024
                    mem_size = info[1].parseInt   #* PAGESIZE # div 1024
                    mem = mem_size - shared_mem
                    cmd = pid_cmdline(i).replace("\x00", "")
                    binpart = cmd.split(" ")[0]
                    binset = [binpart, binpart.split("/")[^1], cmd].toHashSet
                yield (i.int32, pid_name(i), binset,
                        pid_parent(i).int32, mem, shared_mem)
            else:
                discard
    except:
        discard

proc getParents(): ParentInfo =
    var
        parentsize = initOrderedTable[int32, int]()
        procinfo = initTable[int32, (HashSet[string], string)]()
        parents: ParentInfo
    for i in userpids():
        if parentsize.hasKeyOrPut(i.parent, i.mem):
            parentsize[i.parent] += i.mem
        procinfo[i.pid] = (i.cli, i.name)

    for k in parentsize.keys():
        if procinfo.hasKey(k):
            parents.add((parentsize[k], k, procinfo[k][0], procinfo[k][1]))
    return parents.sortedByIt(it.size).reversed

proc send_msg(message: string, urgency: uint8) =
    let bus = getBus(DBUS_BUS_SESSION)
    var msg = makeCall("org.freedesktop.Notifications",
                ObjectPath("/org/freedesktop/Notifications"),
                "org.freedesktop.Notifications",
                "Notify")

    msg.append("Memory tracker")
    msg.append(0'u32)
    msg.append("Available RAM")
    msg.append("Available RAM")
    msg.append(message)
    msg.append(newSeq[string]())
    msg.append({"urgency": newVariant(urgency)}.toTable)
    msg.append(-1'i32)
    discard bus.sendMessage(msg)

proc kill_process(parents: ParentInfo) =
    for i in parents:
        if intersection(whitelist, i.paths).len == 0:
            #discard execProcess("kill $1" % [ $ i.pid])
            if kill(i.pid, 1) == 0:
                let
                    avail = psutil.virtual_memory().percent
                    msg = "Killed PID $1. Matched process in {$2}. Available $3 %." %
                        [ $ i.pid, join(i.paths.toSeq, ", "),
                                fmt"{avail :>0.1f}"]
                errLine msg
                send_msg(msg, 0'u8)
                break

proc progress(usage: auto): int =
    let message = "Used: " & fmt"{usage.percent :>0.1f}" & " %."
    var
        urgency: uint8
        interval = 0
    if usage.percent > config.critical:
        urgency = 2
        interval = 1000
        send_msg(message, urgency)
        let parents = getParents()
        kill_process(parents)
    elif usage.percent > config.warning:
        urgency = 1
        send_msg(message, urgency)
        interval = 5000
    else:
        urgency = 0
        send_msg(message, urgency)
        interval = 5000
    return interval

proc main() =
    loadConf()
    var interval: int
    while true:
        let memory_used = psutil.virtual_memory()
        if memory_used.percent > config.notification:
            interval = progress(memory_used)
        else:
            interval = 1500
        sleep(interval)

when isMainModule:
    main()

