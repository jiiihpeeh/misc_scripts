 
import httpclient
import json
import times
import tables
import os
import math
import db_sqlite

type
  JSONTable = Table[string, Table[string, int64]]

proc initJSONTable(): JSONTable =
  result = initTable[string, Table[string, int64]]() 

const 
    turl ="http://192.168.2.227"
    dbfile = joinPath(getHomeDir(), "/tempmonitor/temp_nim_.db")

var 
    bme = initTable[string, int64]()
    dht = initTable[string, int64]()
    timetable = initTable[string,int64]()
    jsont = initJSONTable()
    text : string
    start : float64 
    timestamp, stime : int

    result, sql_id: int64
    name : string

let db = open(dbfile, "", "", "")
db.exec(sql"""CREATE TABLE IF NOT EXISTS tempinfo (id integer, data json)""")
db.close()


while true:
    start = epochTime()

    var client = newHttpClient()
    try:
        text = client.getContent(turl)
        let jsonNode = parseJson(text)
        timestamp = int(epochTime()*1000)
        timetable["epoch"]=timestamp

        for i in keys(jsonNode):
            try:
                result = int64(jsonNode[i].getFloat()*1000)
                name = i[1 .. len(i)-1]
                case i[0]
                of 'b':
                    bme[name] = result
                of 'd':
                    dht[name] = result
                else:
                    echo "None"
            except:
                echo "?"

        jsont["DHT22"] = dht
        jsont["BME680"] = bme
        jsont["time"] = timetable
        sql_id = int64(round(timestamp/1000))
        #echo jsont
        var jstr = %* jsont
        echo jstr
        let db = open(dbfile, "", "", "")
        db.exec(sql"""INSERT INTO tempinfo VALUES (?, ?)""", sql_id, jstr)
        db.close()
        stime = int((60 - (epochTime() - start))*1000)
        echo stime
        sleep(stime)
    except:
        sleep(5000)

